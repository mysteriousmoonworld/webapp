angular.module('yapp')
    .controller('DownloadSoftCtrl', function($scope,$http,$state,$stateParams,$rootScope,ngDialog,notificationFactory,$mdToast)
    {
        $scope.getfile = function() {
            $http.get('http://localhost:8080/static/' + $stateParams.id,
                {
                    headers: {
                        'X-Auth': sessionStorage.getItem('X-Auth'),
                        'Content-Type': 'application/json',
                        accept: 'base64'
                    },
                    responseType: 'arraybuffer',
                    cache: false

                    //application/zip
                })
                .success(function (data, status, headers, config) {
                    var zip = null;
                    if (data) {
                        console.log(headers("Content-Type"));
                        zip = new Blob([data], {type: headers("Content-Type")});
                    }
                    switch (headers("Content-Type")) {
                        case "application/zip":
                            saveAs(zip, $scope.softdetail.name + ".zip");
                            break;
                        case "application/x-msdownload":
                            saveAs(zip, str_rand() + ".exe");
                            break;
                        case "application/x-tar":
                            saveAs(zip, $scope.softdetail.name + ".tar");
                            break;
                        case "application/x-gzip":
                            saveAs(zip, $scope.softdetail.name + ".tar.gz");
                            break;
                        case "application/x-newton-compatible-pkg":
                            saveAs(zip, $scope.softdetail.name + ".pkg");
                            break;
                    }
                })
                , function () {
                alert("error")
            }
        }
        $scope.showSimpleToast = function() {
            $mdToast.show(
                $mdToast.simple()
                    .textContent('Simple Toast!')
                    .hideDelay(100000)
            );
        };
        $scope.closeToast = function() {
            $mdToast.hide();
        };
        $scope.showOtherVersions=false;
        $rootScope.issue={};
        $scope.mainupdates=[
            {
                platform:"Microsoft Windows",
                reqs:"Windows XP or later, Intel 64-bit processor",
                filename:"go1.6.windows-amd64.msi",
                size:"71MB",
                checksum:"SHA256: 9e185fe7985505e3a65633f5e4db76664607f67f8331f0ce4986ba69b51015b7"
            },
            {
                platform:"Apple OS X",
                reqs:"OS X 10.8 or later, Intel 64-bit processor",
                filename:"go1.6.darwin-amd64.pkg",
                size:"81MB",
                checksum:"SHA256: cabae263fe1a8c3bb42539943348a69f94e3f96b5310a96e24df29ff745aaf5c"
            }
        ]
    })
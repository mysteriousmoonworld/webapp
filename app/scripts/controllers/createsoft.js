/**
 * Created by Alex on 07.03.2016.
 */
angular.module('yapp')
    .controller('CreateSoftCtrl', function($q,$scope, $location,$http,$state,$stateParams,$rootScope,ngDialog,popup,notificationFactory) {
        var config = {
            headers : {'X-Auth':sessionStorage.getItem('X-Auth')},
        };
        $rootScope.hidenav=false;
        $rootScope.modeCreate = true;
        $rootScope.soft={};
        $http({
            url: 'http://localhost:8080/category',
            method: "GET",
            headers: {'X-Auth': sessionStorage.getItem('X-Auth')}
        })
            .success(function (response) {
                if(response.length<1){
                    $scope.categorylistnotempty=false;
                }
                else{
                    $scope.categorylistnotempty=true;
                    $scope.categories=response;
                    $scope.soft.category=$scope.categories[0];
                    if($stateParams.id!=null){
                        $scope.idsoft=$stateParams.id;
                        $rootScope.modeCreate=false;
                        $http.get('http://localhost:8080/soft/'+$stateParams.id+'',config)
                            .then(function(response) {

                                $rootScope.soft=response.data;
                                $scope.soft.category=$scope.categories[0];//TODO $scope.soft.cat.id когда при запросе будет выдаваться категория
                            },
                            function(response) { // optional
                                console.log(response);
                                popup.alertPopup();
                                $rootScope.state=" Отсутствие активности, пожалуйста, авторизуйтесь заново";
                                // failed
                            });

                    }
                }
            },function (response) {
                notificationFactory.error("Что то пошло не так");

            })

        $scope.saveEdit=function (soft) {
            $http({
                url: 'http://localhost:8080/soft/'+$stateParams.id,
                method: "PUT",
                data: {"name": $scope.soft.name,"category":$scope.soft.category.id,'description':$scope.soft.description},
                headers : {'X-Auth':sessionStorage.getItem('X-Auth')}
            })
                .then(function(response) {
                    // success
                    console.log(response);
                    notificationFactory.info("Изменено успешно!")
                    $rootScope.soft={};
                    $state.go("softs");

                }),
                function (response) {
                    console.log(response);
                    notificationFactory.error(response);

                }
        }
        $scope.createUpdate=function () {
            $http({
                url: 'http://localhost:8080/soft/'+$stateParams.id+'/update',
                method: "POST",
                data: {"version": $scope.soft.name_version,'description':$scope.soft.desc},
                headers : {'X-Auth':sessionStorage.getItem('X-Auth')}
            })
                .then(function(response) {
                    // success
                    console.log(response);
                    sessionStorage.setItem('version',response.data.version);
                    notificationFactory.info("Ветка для обновления " +$scope.soft.name_version+" создана!")
                    $rootScope.soft={};
                    $state.go("softs.update",{id:$rootScope.idsoft,id_update:response.data.id});

                }),
                function (response) {
                    console.log(response);
                    notificationFactory.error(response);

                }
        }
        $scope.openModal=false;
        $scope.saveSoft = function () {
                $http({
                    url: 'http://localhost:8080/category/'+$scope.soft.category.id+'/soft',
                    method: "POST",
                    data: {"name": $scope.soft.name, "description": $scope.soft.description},
                    headers: {'X-Auth': sessionStorage.getItem('X-Auth')}
                })
                    .success(function (response) {
                        $rootScope.idsoft = response.id;
                        $rootScope.namesoft = response.name;
                        document.forms.saveForm.reset();
                        $scope.openModal = true;
                        var el = angular.element("#save");
                        el.attr("href", '#demoModal');
                        angular.element("#save").attr("modal");
                    },
                    function (response) { // optional
                        console.log("failed!");
                        notificationFactory.error(response.message);
                        // failed
                    })
                    .error(function (response) {
                        notificationFactory.error(response.message);/*
                     popup.alertPopup();
                     $rootScope.state=" Отсутствие активности, пожалуйста, авторизуйтесь заново";*/
                    });

        }
        $scope.remaining = function () {
            if (255 - $scope.text.length >= 0)
                return 255 - $scope.text.length;
            else
                return -(255 - $scope.text.length);
        };
        $scope.shouldWarn = function () {
            return $scope.text.length > 255;
        };
    })
        .controller('UploadUpdateCtrl', function($scope, $location,$http,$state,$stateParams,notificationFactory,$rootScope){
        $http.get('http://localhost:8080/soft/'+$stateParams.id,{headers: {'X-Auth':sessionStorage.getItem('X-Auth')}})
            .then(function(data){
                $scope.name=data.data.name;
            })
            $rootScope.hidenav=false;
            $scope.version=sessionStorage.getItem('version');
            $scope.request = {};
            $scope.platforms=[
                {
                    key:1,
                    platform: "Darwin",
                    arches:
                        [
                            {
                                id:1,
                                'is_default': 1,
                                title:"386"
                            },
                            {
                                id:2,
                                title:"amd64"
                            },
                            {
                                id:3,
                                title:"arm"
                            },
                            {
                                id:4,
                                title:"arm64"
                            }
                        ]
                },
                {
                    key:2,
                    platform: "DragonFly",
                    arches:
                        [
                            {
                                id:5,
                                'is_default': 1,
                                title:"amd64"
                            }
                        ]
                },
                {
                    key:3,
                    platform: "FreeBSD",
                    arches:
                        [
                            {
                                id:6,
                                'is_default': 1,
                                title:"386"
                            },
                            {
                                id:7,
                                title:"amd64"
                            },
                            {
                                id:8,
                                title:"arm"
                            }
                        ]
                },
                {
                    key:4,
                    platform: "Linux/Debian",
                    arches:
                        [
                            {
                                id:9,
                                'is_default': 1,
                                title:"386"
                            },
                            {
                                id:10,
                                title:"amd64"
                            },
                            {
                                id:11,
                                title:"arm"
                            },
                            {
                                id:12,
                                title:"arm64"
                            },
                            {
                                id:13,
                                title:"ppc64"
                            },
                            {
                                id:14,
                                title:"ppc64le"
                            },
                            {
                                id:15,
                                title:"mips64"
                            },
                            {
                                id:16,
                                title:"mips64le"
                            }
                        ]
                },
                {
                    key:5,
                    platform: "NetBSD",
                    arches:
                        [
                            {
                                id:17,
                                'is_default': 1,
                                title:"386"
                            },
                            {
                                id:18,
                                title:"amd64"
                            },
                            {
                                id:19,
                                title:"arm"
                            }
                        ]
                },
                {
                    key:6,
                    platform: "OpenBSD",
                    arches:
                        [
                            {
                                id:20,
                                'is_default': 1,
                                title:"386"
                            },
                            {
                                id:21,
                                title:"amd64"
                            },
                            {
                                id:22,
                                title:"arm"
                            }
                        ]
                },
                {
                    key:7,
                    platform: "Plan9",
                    arches:
                        [
                            {
                                id:23,
                                'is_default': 1,
                                title:"386"
                            },
                            {
                                id:24,
                                title:"amd64"
                            },
                        ]
                },
                {
                    key:8,
                    platform: "Solaris",
                    arches:
                        [
                            {
                                id:25,
                                'is_default': 1,
                                title:"amd64"
                            }
                        ]
                },
                {
                    key:9,
                    platform: "Windows",
                    arches:
                        [
                            {
                                id:26,
                                'is_default': 1,
                                title:"x32(x86)"
                            },
                            {
                                id:27,
                                title:"x64"
                            }
                        ]

                }
            ]
        $scope.request.platform = $scope.platforms[0];
        $scope.request.arch = $scope.platforms[0].arches[0];
        $scope.setDefaultArch = function() {
                angular.forEach($scope.request.platform.arches, function(arch) {	// Сейчас в request находится вольер, клетки которого нужно отобразить
                    if (arch.is_default) {
                        $scope.request.arch = arch;								// Перебираем все клетки этого вольера и записываем в модель клетку по умолчанию, это избавит от пустого option во втором select
                    }
                });
            };

            var currentTime = new Date();
            $scope.currentTime = currentTime;
            $scope.myDate = new Date();
            $scope.myDate.format='dd/mm/yy';
            $scope.minDate = new Date(
                $scope.myDate.getFullYear(),
                $scope.myDate.getMonth() - 2,
                $scope.myDate.getDate());
            $scope.maxDate = new Date(
                $scope.myDate.getFullYear(),
                $scope.myDate.getMonth() + 2,
                $scope.myDate.getDate());
            $scope.onlyWeekendsPredicate = function(date) {
                var day = date.getDay();
                return day === 0 || day === 6;
            }
        $scope.uploadFile = function() {
            var file = $scope.myFile;
            console.log(file);
            var name =file.name;
            var os =$scope.request.arch.id;
            var ver =$scope.version;
            var fd = new FormData();
            var uploadUrl = 'http://localhost:8080/soft/'+$stateParams.id+'/update/'+$stateParams.id_update+'/os/'+os+'/build';
            fd.append('filename', name);
            fd.append('upload', file);
            var send = $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined,'X-Auth':sessionStorage.getItem('X-Auth')}
            });
            send.success(function(data, status, headers, config) {
                    // success
                    if(status==201){$scope.status="Файл "+file.name+" успешно загружен на сервер!"
                                    notificationFactory.success($scope.status)};
                },
                send.error(function(data, status, headers, config) { // optional
                    notificationFactory.error();
                    return "Ошибка в запросе :(";
                }));




        };
    })
    .directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }])


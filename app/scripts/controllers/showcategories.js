/**
 * Created by Alex on 07.03.2016.
 */
'use strict';

angular.module('yapp')
    .controller('ShowCategoriesCtrl',function($scope, $rootScope, usersFactory, notificationFactory,$http,$mdDialog,$mdMedia,popup) {
        var config = {
            headers : {'X-Auth':sessionStorage.getItem('X-Auth')},
        };
        $rootScope.showLegend=false;
        $rootScope.hidenav=false;
        if($rootScope.progressbar!=undefined){
            $rootScope.progressbar.reset();
        }
        var isDirty = function(item) {
            return item.name != item.serverName||item.description != item.serverDescription;
        }
        $scope.getCategories=function () {
            $http({
                url: 'http://localhost:8080/category',
                method: "GET",
                headers: {'X-Auth': sessionStorage.getItem('X-Auth')}
            })
                .success(function (response) {
                    if(response.length<1){
                        notificationFactory.info("Похоже категорий нет");
                        $scope.categorylistnotempty=false;
                    }
                    else{
                        $scope.categorylistnotempty=true;
                        $scope.categories=response;
                    }
                },function (response) {
                    notificationFactory.error("Что то пошло не так");

                })
                .error(function (response) {
                    console.log(response);
                    popup.alertPopup();
                    $rootScope.state=" Отсутствие активности, пожалуйста, авторизуйтесь заново";
                })


        }
        $scope.getCategories();
        $scope.toggleEditMode = function (item) {
            // Toggle
            item.editMode = !item.editMode;
            // if item is not in edit mode anymore
            if (!item.editMode) {
                // Restore name
                item.name = item.serverName;
                item.description = item.serverDescription;
            } else {
                // save server name to restore it if the user cancel edition
                item.serverName = item.name;
                item.serverDescription = item.description;

                // Set edit mode = false and restore the name for the rest of items in edit mode
                // (there should be only one)
                $scope.categories.forEach(function (i) {
                    // item is not the item being edited now and it is in edit mode
                    if (item.id != i.id && i.editMode) {
                        // Restore name
                        i.name = i.serverName;
                        i.description = i.serverDescription;
                        i.editMode = false;
                    }
                });
            }

        };
        $scope.loading = false;
        $scope.updateCategory = function (item) {
            var category =item;
            // Only update if there are changes
            if (isDirty(item)) {
                $http({
                    url:"http://localhost:8080/category/"+item.id,
                    method:"PUT",
                    data:{"name":item.name,"description":item.description},
                    headers:{'X-Auth': sessionStorage.getItem('X-Auth')}
                })
                    .error(function (response) {
                        notificationFactory.error(response);
                    })
                    .then(function () {

                            var text= 'Информация о категории '+ category.name.toUpperCase() + ' успешно обновлена!';
                            notificationFactory.success(text);

                        category.editMode = false;
                    },function (response) {
                        console.log(response);
                        notificationFactory.error();
                    }
                )

                item={};
                /*
                 usersFactory.update({ id: item.id }, item, function (success) {
                 requestSuccess();
                 }, requestError);*/
            }
        }

    })


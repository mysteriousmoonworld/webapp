/**
 * Created by Asus on 01.04.2016.
 */
angular.module('yapp')
    .factory('StateManager', function StateManager($rootScope, $log) {

    var stateContainer = [];

    return {
        add: function (service) {
            stateContainer.push(service);
            $rootScope.globalLoader = true;
            $log.log('Add service: ' + service);
        },

        remove: function (service) {
            stateContainer = _.without(stateContainer, service);

            if (stateContainer.length === 0) {
                $rootScope.globalLoader = false;
            }

        },

        getByName: function (service) {
            return _.include(stateContainer, service)
        },

        clear: function () {
            stateContainer.length = 0;
            return true;
        }
    }

});
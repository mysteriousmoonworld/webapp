/**
 * Created by Asus on 12.04.2016.
 */
angular.module('yapp')
.factory('notificationFactory', function (toastr,$sce) {
    return {
        success: function (text) {
            if (text === undefined) {
                text = '';
            }
            toastr.success(text,"Успех!");
        },
        info:function (text) {
            if (text === undefined) {
                text = '';
            }
            toastr.info(text,"Уведомление:");
        },
        error: function (text) {
            if (text === undefined) {
                text = '';
            }
            toastr.error(text,"Ошибка!",{positionClass: 'toast-bottom-right'});
            console.log(toastr.options)
            
        },
    };
})
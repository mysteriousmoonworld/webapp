'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('LoginCtrl', function($scope, $http,$rootScope,$state,StateManager,ngProgressFactory,$sce,$mdToast) {
      if($rootScope.progressbar!=undefined){
          $rootScope.progressbar.reset();
      }
    localStorage.clear();
    sessionStorage.clear();
    $rootScope.dark=false;
    $scope.login = function() {

        $http({
            url: 'http://localhost:8080/login',
            method: "POST",
            data: { "email":$scope.email,"pass":$scope.password}
        })
            .error(function (response) {
                var text;
                if(response==null){
                    text=$sce.trustAsHtml("<br>Сервер недоступен. <br> Попробуйте обновить страницу и повторить запрос позднее");
                    var text=$sce.trustAsHtml("<i class='material-icons'>error_outline</i>&nbsp;<strong class='center'>Ошибка! </strong><br> Сервер недоступен.Попробуйте обновить страницу и повторить запрос позднее");
                    ohSnap(text, {color: 'red', icon: 'icon-alert'});
                }
                else{
                    var text=$sce.trustAsHtml("<i class='material-icons'>error_outline</i>&nbsp;<strong class='center'>Ошибка! </strong><br> Логин или пароль не верные!");
                    ohSnap(text, {color: 'red', icon: 'icon-alert'});
                }

                console.log(response);
            })
            .then(function(response) {
                // success

                switch (response.data.role) {
                    case "Managers":
                        $state.go("managerDashboard");
                        break;
                    case "Clients":
                        $state.go("userDashboard");
                        break;
                    case "Administrators":
                        $state.go("adminDashboard");
                        break;
                    case "Providers":
                        $state.go("adminDashboard");
                        break;

                }
                var userdata={email: response.data.email, role:response.data.role,
                id:response.data.id};
                sessionStorage.setItem('X-Auth',response.data.token);
                $rootScope.userData=angular.toJson(userdata);
                localStorage.setItem('userData',angular.toJson(response.data));


            },
            function(response) { // optional
                $scope.warning="Логин или пароль не верные!";
                // failed
            });
      return false;
    }

  });

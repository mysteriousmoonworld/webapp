/**
 * Created by Asus on 06.04.2016.
 */
angular.module('yapp')
    .factory("usersFactory", function ($resource) {
        return $resource('http://localhost:8080/users/:id',
            {
                // default URL params
                // @ Indicates that the value should be obtained from a data property
                id: '@Id',

            },
            {
                // add update to actions (is not defined by default)
                'update': { method: 'PUT' ,
                            headers: { 'X-Auth': sessionStorage.getItem('X-Auth')  }
                }
            });
    })
    .controller("UsersCtrl", function ($scope, $rootScope, usersFactory, notificationFactory,$http,$mdDialog,$mdMedia,popup) {
        $rootScope.showLegend=true;
        $scope.editRole=false;

        // PRIVATE FUNCTIONS
        var requestSuccess = function () {
            notificationFactory.success();
        }

        var requestError = function () {
            notificationFactory.error();
        }

        var isNameDuplicated = function (itemName) {
            return $scope.users.some(function (entry) {
                return entry.email == itemName;
            });
        };

        var isDirty = function(item) {
            return item.role != item.serverRole;
        }
        var isDirtyEmail = function(item) {
            return item.email != item.serverEmail;
        }

        // PUBLIC PROPERTIES

        // all the items
        $scope.getUsers=function () {
            $http({
                url: 'http://localhost:8080/users',
                method: "GET",
                headers: {'X-Auth': sessionStorage.getItem('X-Auth')}
            })
                .success(function (response) {
                        $scope.users=response;

                },function (response) {
                    notificationFactory.error("Что то пошло не так");

                })
                .error(function (response) {
                    console.log(response);
                    popup.alertPopup();
                    $rootScope.state=" Отсутствие активности, пожалуйста, авторизуйтесь заново";
                })


        }
        $scope.getUsers();
        $scope.data = {
            availableOptions: [
                {id: 1, name: 'Administrators'},
                {id: 2, name: 'Providers'},
                {id: 3, name: 'Managers '},
                {id: 4, name: 'Clients'}
            ],
            selectedOption: {} //This sets the default value of the select in the ui
        };
         // Toggle an item between normal and edit mode
        $scope.toggleEditRole = function (item) {
            // Toggle
            $scope.data.selectedOption={id:item.role.id,name:item.role.name};
            item.editRole = !item.editRole;
            // if item is not in edit mode anymore
            if (!item.editRole) {
                // Restore name
                item.role.name = item.serverRole;
            } else {
                // save server name to restore it if the user cancel edition
                item.serverRole = item.role.name;

                // Set edit mode = false and restore the name for the rest of items in edit mode
                // (there should be only one)
                $scope.users.forEach(function (i) {
                    // item is not the item being edited now and it is in edit mode
                    if (item.id != i.id && i.editRole) {
                        // Restore name
                        i.role.name = i.serverRole;
                        i.editRole = false;
                    }
                });
            }

        };
        $scope.saveEditRole =function (user) {
            var userSession=angular.fromJson(localStorage.getItem('userData'));
            if(user.id==userSession.id){
                notificationFactory.error("Вы не можете редактировать свои права!");
                user.editRole = false;
            }
            else{
                if (isDirty(user)) {
                    $http({
                        url: "http://localhost:8080/users/" + user.id + "/role",
                        method: "PUT",
                        data: {"Name": $scope.data.selectedOption.name},
                        headers: {'X-Auth': sessionStorage.getItem('X-Auth')}
                    })
                        .success(function (response) {
                                notificationFactory.success("Пользователю " + user.email + " назначены права " + $scope.data.selectedOption.name);
                                user.editRole = false;
                            }, function (response) {
                                notificationFactory.error(response.data.message);
                            }
                        )
                        .error(function(response){
                            notificationFactory.error(response.message);
                        })
                }
                else {
                    notificationFactory.info("Полномочия" + user.email + " не были изменены ");
                    user.editRole = false;
                }
            }
            
        }
        $scope.changeSelect = function (user) {
            $scope.user=user;
            $scope.user.role.name=$scope.data.selectedOption.name;
         }
        $scope.toggleEditMode = function (item) {
            // Toggle
            item.editMode = !item.editMode;
            // if item is not in edit mode anymore
            if (!item.editMode) {
                // Restore name
                item.email = item.serverEmail;
                item.name = item.serverName;
            } else {
                // save server name to restore it if the user cancel edition
                item.serverEmail = item.email;
                item.serverName = item.name;

                // Set edit mode = false and restore the name for the rest of items in edit mode
                // (there should be only one)
                $scope.users.forEach(function (i) {
                    // item is not the item being edited now and it is in edit mode
                    if (item.id != i.id && i.editMode) {
                        // Restore name
                        i.email = i.serverEmail;
                        i.name = i.serverName;
                        i.editMode = false;
                    }
                });
            }

        };
        // the item being added
        $scope.newUser = { };
        // indicates if the view is being loaded
        $scope.loading = false;
        // indicates if the view is in add mode
        $scope.addMode = false;
        // PUBLIC FUNCTIONS

        // Toggle the grid between add and normal mode
        $scope.toggleAddMode = function () {
            $scope.addMode = !$scope.addMode;

            // Default new item name is empty
            $scope.newUser.id=$scope.users.length+1;
            $scope.newUser.role=$scope.data.selectedOption.name;
            $scope.today = function() {
                $scope.newUser.date = new Date();
            };
            $scope.today();

            $scope.clear = function() {
                $scope.newUser.date = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            $scope.dateOptions = {
                dateDisabled: disabled,
                formatYear: 'yy',
                maxDate: new Date(2020, 5, 22),
                minDate: new Date(),
                startingDay: 1

            };
            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                    mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            $scope.toggleMin = function() {
                $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
                $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            };
            $scope.toggleMin();

            $scope.open1 = function() {
                $scope.popup1.opened = true;
            };

            $scope.setDate = function(year, month, day) {
                $scope.newUser.date= new Date(year, month, day);
            };

            $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            $scope.altInputFormats = ['M!/d!/yyyy'];


            $scope.popup1 = {
                opened: false
            };


            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0,0,0,0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }
        };



        // Creates the 'newItem' on the server
        $scope.createItem = function () {

            // Check if the item is already on the list
            var duplicated = isNameDuplicated($scope.newUser.email);

            if (!duplicated) {
                $scope.newUser.id=$scope.users.length+1;
                $scope.toggleAddMode();
                $scope.users.push($scope.newUser);
                var text= 'Пользователь '+$scope.newUser.email.toUpperCase() + ' успешно добавлен'
                notificationFactory.success(text);
                $scope.newUser={};

                /*
                usersFactory.save($scope.newUser,
                    // success response
                    function (createdItem) {
                        // Add at the first position
                        $scope.users.unshift(createdItem);
                        $scope.toggleAddMode();

                        requestSuccess();
                    },
                    requestError);*/
            } else {
                notificationFactory.error("The item already exists.");
            }

        }


        // Gets an item from the server using the id
        $scope.readItem = function (itemId) {
            usersFactory.get({ id: itemId }, requestSuccess, requestError);
        }

        // Updates an item
        $scope.updateUser = function (item,ev) {
            var userSession=angular.fromJson(localStorage.getItem('userData'));
            var user =item;
            // Only update if there are changes
                $http({
                    url:"http://localhost:8080/users/"+item.id,
                    method:"PUT",
                    data:{"email":item.email,"name":item.name,"role":item.role},
                    headers:{'X-Auth': sessionStorage.getItem('X-Auth')}
                })
                    .error(function (response) {
                        notificationFactory.error(response);
                    })
                    .then(function () {
                            if(user.id==userSession.id&&isDirtyEmail(item)){
                                $rootScope.hidenav=true;
                                var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
                                $mdDialog.show({
                                        controller:'UsersCtrl',
                                        templateUrl: '../views/admin/warning_user.html',
                                        parent: angular.element(document.body),
                                        targetEvent: ev,
                                        clickOutsideToClose:true,
                                        fullscreen: useFullScreen
                                    })
                                    .then(function(answer) {
                                        $scope.status = 'You cancelled the dialog.';
                                    }, function() {
                                        $scope.status = 'You cancelled the dialog.';
                                    });
                                $scope.$watch(function() {
                                    return $mdMedia('xs') || $mdMedia('sm');
                                }, function(wantsFullScreen) {
                                    $scope.customFullscreen = (wantsFullScreen === true);
                                });
                                
                            }
                            else{
                                var text= 'Информация о пользователе '+ user.email.toUpperCase() + ' успешно обновлена!';
                                notificationFactory.success(text);
                            }

                            user.editMode = false;
                        },function (response) {
                            console.log(response);
                            notificationFactory.error();
                        }
                    )

                item={};
                /*
                usersFactory.update({ id: item.id }, item, function (success) {
                    requestSuccess();
                }, requestError);*/

        }
        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
            $rootScope.hidenav=false;
        };
        $scope.hide = function() {
            $mdDialog.hide();
            $rootScope.hidenav=false;
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
            $rootScope.hidenav=false;
        };
        // Deletes an item
        $scope.deleteItem = function (item) {
            var title = "Удалить '" + item.email + "'";
            var msg = "Вы действительно уверены?";
            var confirmCallback = function () {
                usersFactory.delete({ id: item.id }, item, function (success) {
                    requestSuccess();
                    // Remove from scope
                    var index = $scope.users.indexOf(item);
                    $scope.users.splice(index, 1);
                }, requestError);
            };

            modalWindowFactory.show(title, msg, confirmCallback);

        }

        // Get all items from the server
        $scope.getAllItems = function () {/*
            $scope.loading = true;
            $scope.users = usersFactory.query(function (success) {
                $scope.loading = false;
            }, requestError);*/
        };

        // In edit mode, if user press ENTER, update item
        $scope.updateOnEnter = function (item, args) {
            // if key is enter
            if (args.keyCode == 13) {
                $scope.updateItem(item);
                // remove focus
                args.target.blur();
            }
        };

        // In add mode, if user press ENTER, add item
        $scope.saveOnEnter = function (item, args) {
            // if key is enter
            if (args.keyCode == 13) {
                $scope.createItem();
                // remove focus
                args.target.blur();
            }
        };
        $scope.openDatePicker=function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            self.datePickerOpen = true;
        };


        // LOADS ALL ITEMS
        $scope.getAllItems();
    })
    .controller("UsersCreateCtrl", function ($scope, usersFactory, $http, notificationFactory,modalWindowFactory) {
        $scope.email="";
        $scope.reg = function() {

            $http({
                url: 'http://localhost:8080/register',
                method: "POST",
                data: { "email":$scope.email,"pass":$scope.password }
            })
                .then(function(response) {
                        // success
                        notificationFactory.success("Пользователь добавлен!")
                    },
                    function(response) { // optional

                        $scope.warning="Такой пользователь уже существует";
                        // failed
                    });

            return false;
        }
    });


'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('DashboardCtrl', function($scope, $state,StateManager,$rootScope,ngProgressFactory,$http,notificationFactory) {
      $rootScope.progressbar = ngProgressFactory.createInstance();
      $rootScope.progressbar.setColor('#5bc0de');
      $rootScope.progressbar.reset();
      var config = {
            headers : {'X-Auth':sessionStorage.getItem('X-Auth')},
        };
      $scope.dark=true;
    try {
        var user=angular.fromJson(localStorage.getItem('userData'));
        $scope.email=user.email;/*

        $http.get('http://localhost:8080/users/'+user.id,config)
            .success(function(response){
                $scope.user=response;
                $scope.nick=$scope.user.name;
            })
            .error(function(response){
                notificationFactory.error(response);
            })
*/
        $scope.ser=user;
        $rootScope.showLinkToUploadUpdate=(user.permissions.create_soft_update==true)?true:false;
        $scope.editUsers=(user.role=="Providers"||user.role=="Clients"||user.role=="Managers")?false:true;
        $rootScope.showLegend=false;
    }
    catch (err){
        $state.go('login');
    }
    $scope.showprofile=false;
      if($scope.showprofile){
          $scope.show='block';
      }
    $rootScope.hidenav=false;
    $scope.cancel=function () {
        $rootScope.showLegend=false;
    }
        $scope.info = detect.parse(navigator.userAgent);
        $scope.platform=platform.os.family.toLowerCase() + " amd"+platform.os.architecture;
  })
.controller('UserDashboardCtrl', function($scope, $state,$rootScope,ngProgressFactory) {
    $rootScope.progressbar = ngProgressFactory.createInstance();
    $rootScope.progressbar.setColor('#33691e');
    $rootScope.dark=true;
    $scope.active=false;
    $rootScope.progressbar.reset();
    try {
        var user=angular.fromJson(localStorage.getItem('userData'));
        $scope.email=user.email;
        $rootScope.showLinkToUploadUpdate=(user.permissions.create_soft_update==true)?true:false;
        $scope.editUsers=(user.role=="Providers"||user.role=="Clients"||user.role=="Managers")?false:true;
        $rootScope.showLegend=false;
        $scope.user=user;
    }
    catch (err){
        $state.go('login');
    }
    $scope.showprofile=false;
    if($scope.showprofile){
        $scope.show='block';
    }
    $rootScope.hidenav=false;
    $scope.cancel=function () {
        $rootScope.showLegend=false;
    }

    $scope.info = detect.parse(navigator.userAgent);
    $scope.platform=platform.os.family +' '+ platform.os.version + " x"+platform.os.architecture;


    


})
    .controller('ManagerDashboardCtrl', function($scope, $state,$rootScope,$http,ngProgressFactory,notificationFactory,popup) {
        var config = {
            headers : {'X-Auth':sessionStorage.getItem('X-Auth')},
        };
        $rootScope.progressbar = ngProgressFactory.createInstance();
        $rootScope.progressbar.setColor('#33691e');
        $rootScope.dark=true;
        $scope.active=false;
        $rootScope.progressbar.reset();
        try {
            var user=angular.fromJson(localStorage.getItem('userData'));
            $scope.email=user.email;
            $rootScope.showLinkToUploadUpdate=(user.permissions.create_soft_update==true)?true:false;
            $scope.editUsers=(user.role=="Providers"||user.role=="Clients"||user.role=="Managers")?false:true;
            $rootScope.showLegend=false;
            $scope.user=user;
        }
        catch (err){
            $state.go('login');
        }
        $scope.showprofile=false;
        if($scope.showprofile){
            $scope.show='block';
        }
        $rootScope.hidenav=false;
        $scope.userNotSelected=true;
        $scope.info = detect.parse(navigator.userAgent);
        $scope.platform=platform.os.family +' '+ platform.os.version + " x"+platform.os.architecture;
        $scope.getUsers= function(){
            $http.get('http://localhost:8080/users',config)
                .success(function(response){
                    $scope.users=response;
                })
                .error(function(response){
                    notificationFactory.error(response);
                })
        }
        $scope.getUsers();
        $scope.showSoft=false;
        $scope.showKeys=false;
        $scope.preload=false;
        $scope.choiceUser=function(user) {
            $scope.userNotSelected = false;
            $http.get('http://localhost:8080/users/'+user.id,config)
                .success(function(response){
                    $scope.user=response;
                    $scope.getKeys(user);

                })
                .error(function(response){
                    notificationFactory.error(response);
                })
        }
        $scope.showSoftList=function(user){
            $scope.showKeys=false;
            $scope.showSoft=true;
            $scope.preload=true;
            $http({
                url: 'http://localhost:8080/soft',
                method: "GET",
                headers : {'X-Auth':sessionStorage.getItem('X-Auth')}
            })
                .then(function(response) {

                    $scope.softlist=response.data;
                    $scope.preload=false;
                },
                function(response) { // optional
                    console.log(response);
                    popup.alertPopup();
                    $rootScope.state=" ���������� ����������, ����������, ������������� ������";
                    // failed
                });


        }
        $scope.getKey=function(user,soft){
            $http({
                url: 'http://localhost:8080/keys/users/'+user.id+'/soft/'+soft.id,
                method: "POST",
                headers : {'X-Auth':sessionStorage.getItem('X-Auth')}
            })
                .then(function(response) {

                    notificationFactory.success(response);
                },
                function(response) { // optional
                    notificationFactory.error(response);
                    // failed
                });
        }
        $scope.getKeys=function(user,soft){
            $scope.showSoft=false;
            $http({
                url: 'http://localhost:8080/keys/users/'+user.id,
                method: "GET",
                headers : {'X-Auth':sessionStorage.getItem('X-Auth')}
            })
                .then(function(response) {
                    $scope.keylist=response.data;
                },
                function(response) { // optional
                    notificationFactory.error(response);
                    // failed
                });
        }

    })
    .controller('OverviewCtrl', function($scope, $state,$http) {
      
    });

/**
 * Created by Alex on 07.03.2016.
 */
'use strict';
function str_rand() {
    var result       = '';
    var words        = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
    var max_position = words.length - 1;
    for( var i = 0; i < 5; ++i ) {
       var position = Math.floor ( Math.random() * max_position );
        result = result + words.substring(position, position + 1);
    }
    return result;
}
angular.module('yapp')
        .factory('pagination',function ($sce) {
        var currentPage=0;
        var itemsPerPage=4;
        var products=[];
        var prevPageNum;
        return{
            getSoftList: function() {
                var config = {
                    headers : {'X-Auth':sessionStorage.getItem('X-Auth')}
                };
                return $http.get('http://localhost:8080/soft',config).then(function(response) {
                    return response.data;
                });
            },
            setProducts:function(newProducts){
                products=newProducts;
            },
            getPageProduts:function(num){
                var num =angular.isUndefined(num)?0:num;
                var first=itemsPerPage*num;
                var last=first+itemsPerPage;
                currentPage=num;
                last = last > products.length?(products.length):last;
                return products.slice(first,last);
            },
            numberOfPages:function(){
                return Math.ceil(products.length/itemsPerPage);
            },
            getCurrentPageNum:function(){
                return currentPage;
            },
            getPrevPageSoft:function(){
                prevPageNum=currentPage-1;
                if(prevPageNum<0)
                    prevPageNum=0;
                return this.getPageProduts(prevPageNum);
            },
            getNextPageSoft:function(){
                var nextPageNum=currentPage+1;
                var pagesNum=this.numberOfPages();
                if(prevPageNum>=pagesNum)prevPageNum=pagesNum-1;
                return this.getPageProduts(nextPageNum);
            },
            getPaginationList:function(){
                var pagesNum=this.numberOfPages();
                var paginationList=[];
                paginationList.push({
                    name:$sce.trustAsHtml("&laquo;"),
                    link:'prev'
                });
                for (var i=0;i<pagesNum;i++){
                    var name =i+1;
                    paginationList.push({
                        name:$sce.trustAsHtml(String(name)),
                        link:i
                    });
                };

                paginationList.push({
                    name:$sce.trustAsHtml("&raquo;"),
                    link:'next'
                });
                if(pagesNum>1){
                    return paginationList;
                     }
                else{
                    return null;
                }

            }
        }
    })
    .controller('ShowSoftsCtrl',function($scope, $timeout,$http,pagination,$state,$rootScope,popup,notificationFactory,ngProgressFactory) {
        var config = {
            headers : {'X-Auth':sessionStorage.getItem('X-Auth')},
        };
        $rootScope.showLegend=false;
        $rootScope.hidenav=false;
        if($rootScope.progressbar!=undefined){
            $rootScope.progressbar.reset();
        }
        /**
         * Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний
         * param  iNumber Integer Число на основе которого нужно сформировать окончание
         * param  aEndings Array Массив слов или окончаний для чисел (1, 4, 5),
         *         например ['яблоко', 'яблока', 'яблок']
         * return String
         */
        function getNumEnding(iNumber, aEndings)
        {
            var sEnding, i;
            iNumber = iNumber % 100;
            if (iNumber>=11 && iNumber<=19) {
                sEnding=aEndings[2];
            }
            else {
                i = iNumber % 10;
                switch (i)
                {
                    case (1): sEnding = aEndings[0]; break;
                    case (2):
                    case (3):
                    case (4): sEnding = aEndings[1]; break;
                    default: sEnding = aEndings[2];
                }
            }
            return sEnding;
        }
        $scope.getNumber = function(num) {
            var mark=0;
            if(num>0&&num<10){
                mark=1;
            }
            if(num>=10&&num<20){
                mark=2;
            }
            if(num>=20&&num<30){
                mark=3;
            }
            if(num>=30&&num<40){
                mark=4;
            }
            if(num>=40){
                mark=5;
            }

            return new Array(mark);
        }
        $scope.printSoft = function (response) {
            pagination.setProducts(response.sort(function(obj2, obj1) {
                return parseInt(obj2.id) - parseInt(obj1.id)
            }));
            $rootScope.softlist=pagination.getPageProduts();
            $scope.paginationList = pagination.getPaginationList();
        }
            
            $rootScope.progressbar.start();
            var url;
            if($state.params.id==null){
                url='http://localhost:8080/soft';
            }
                else {
                url='http://localhost:8080/follow/soft';
            }
            $http.get(url,config)
                .success(function(response) {
                    $rootScope.original=response;
                    if(response.length<1){
                        notificationFactory.info("Похоже продуктов нет");
                        $scope.softlistnotempty=false;
                    }
                    else{
                        $scope.softlistnotempty=true;
                        $scope.printSoft(response);

                        $scope.numsoft=$scope.original.length+' '+getNumEnding($scope.original.length,['запись','записи','записей']);
                    }

                    $rootScope.progressbar.complete();

                })

                .error(function (response) {
                    console.log(response);
                    popup.alertPopup();
                    $rootScope.state=" Отсутствие активности, пожалуйста, авторизуйтесь заново";
                })
        $scope.editSoft=function (soft) {
            $rootScope.soft=soft;

        }
        $scope.showPage=function(page){
            if(page=='prev'){
                $rootScope.softlist=pagination.getPrevPageSoft();
            }
            else if (page=='next'){
                $rootScope.softlist=pagination.getNextPageSoft();
            }else{
                $rootScope.softlist=pagination.getPageProduts(page);
            }
        },
        $scope.currentPageNum=function(){
            return pagination.getCurrentPageNum();
        }/*
        var conn = new WebSocket('ws://localhost:8080/notifications?t='+sessionStorage.getItem('X-Auth')+'');
        conn.onopen = function(e) {
            console.log("Ты супер");
        }*/
    })
    .controller('ShowSoftCtrl', function($scope,$http,$stateParams,$state,$rootScope,notificationFactory,popup) {
        $rootScope.progressbar.start();
        $rootScope.hidenav=false;
        $scope.soft = { };
        $rootScope.issue={};
        $scope.sendIssue=function () {
            $http({
                url: 'http://localhost:8080/soft/'+$stateParams.id+'/update/1/issue',
                method: "POST",
                data: {"message": $rootScope.issue.message},
                headers: {'X-Auth': sessionStorage.getItem('X-Auth')}
            })
                .then(function (response) {
                    notificationFactory.success("Ваш отзыв отправлен!");
                },function (response) {
                    notificationFactory.error("Что то пошло не так");

                })


        }
        $scope.getNumber = function(num) {
            var mark=0;
            if(num>0&&num<10){
                mark=1;
            }
            if(num>=10&&num<20){
                mark=2;
            }
            if(num>=20&&num<30){
                mark=3;
            }
            if(num>=30&&num<40){
                mark=4;
            }
            if(num>=40){
                mark=5;
            }

            return new Array(mark);
        }
        $scope.getfile = function() {
            $http.get('http://localhost:8080/static/' + $stateParams.id,
                {
                    headers: {
                        'X-Auth': sessionStorage.getItem('X-Auth'),
                        'Content-Type': 'application/json',
                        accept: 'base64'
                    },
                    responseType: 'arraybuffer',
                    cache: false

                    //application/zip
                })
                .success(function (data, status, headers, config) {
                    var zip = null;
                    if (data) {
                        console.log(headers("Content-Type"));
                        zip = new Blob([data], {type: headers("Content-Type")});
                    }
                    switch (headers("Content-Type")) {
                        case "application/zip":
                            saveAs(zip, $scope.softdetail.name + ".zip");
                            break;
                        case "application/x-msdownload":
                            saveAs(zip, str_rand() + ".exe");
                            break;
                        case "application/x-tar":
                            saveAs(zip, $scope.softdetail.name + ".tar");
                            break;
                        case "application/x-gzip":
                            saveAs(zip, $scope.softdetail.name + ".tar.gz");
                            break;
                        case "application/x-newton-compatible-pkg":
                            saveAs(zip, $scope.softdetail.name + ".pkg");
                            break;
                    }
                })
                , function () {
                alert("error")
            }
        }
        $scope.updates=[
            {   id:"1",
                version:"go 1.6",
                data:
                    [{
                    name:"go1.6.darwin-amd64.pkg",
                    kind:"Installer",
                    os:"OS X",
                    arch:"64-bit",
                    size:"81MB",
                    sha256checksum:"cabae263fe1a8c3bb42539943348a69f94e3f96b5310a96e24df29ff745aaf5c"
                    },
                    {
                        name:"go1.6.darwin-amd64.pkg",
                        kind:"Installer",
                        os:"OS X",
                        arch:"64-bit",
                        size:"81MB",
                        sha256checksum:"cabae263fe1a8c3bb42539943348a69f94e3f96b5310a96e24df29ff745aaf5c"
                    }]

            },
            {
                id:"2",
                version:"go 1.5",
                data:
                    [{
                        name:"go1.5.darwin-amd64.pkg",
                        kind:"Installer",
                        os:"OS X",
                        arch:"64-bit",
                        size:"81MB",
                        sha256checksum:"cabae263fe1a8c3bb42539943348a69f94e3f96b5310a96e24df29ff745aaf5c"
                    },
                        {
                            name:"go1.5.1.darwin-amd64.pkg",
                            kind:"Installer",
                            os:"OS X",
                            arch:"64-bit",
                            size:"81MB",
                            sha256checksum:"cabae263fe1a8c3bb42539943348a69f94e3f96b5310a96e24df29ff745aaf5c"
                        }]
            }
            
        ]
            $http({
            url: 'http://localhost:8080/soft/'+$stateParams.id+'',
            method: "GET",
            headers : {'X-Auth':sessionStorage.getItem('X-Auth')}
        })
            .then(function(response) {

                $scope.softdetail=response.data;
                $scope.mark = $scope.getNumber($scope.softdetail.count_likes);
                $scope.idsoft=$stateParams.id;
                $rootScope.progressbar.complete();
            },
            function(response) { // optional
                console.log(response);
                popup.alertPopup();
                $rootScope.state=" Отсутствие активности, пожалуйста, авторизуйтесь заново";
                // failed
            });
        $scope.checkAccessToUpload =function (soft) {
            if(localStorage.getItem('userData').id!=soft.user_id){
                notificationFactory.error("Вы не можете загружать обновления для этого софта")
            }
            else
            {
                if($scope.soft.name_version==undefined){
                    $state.go("softs.update",{id:$stateParams.id});
                }else {
                    $scope.createUpdate($scope.soft.name_version,$scope.soft.description);
                }

            }
            
        }
        $scope.addUpdate=false;
        $scope.showfiles=false;
        $scope.getupdates=function () {
            $http({
                url: 'http://localhost:8080/soft/'+$stateParams.id+'/updates',
                method: "GET",
                headers : {'X-Auth':sessionStorage.getItem('X-Auth')}
            })
                .then(function(response) {
                    // success
                    $scope.releaseslist=response.data;
                })
        }
        $scope.putLike = function(){
            $http({
                url: 'http://localhost:8080/soft/'+$state.params.id+'/like',
                method: "PUT",
                headers: {'X-Auth': sessionStorage.getItem('X-Auth')}
            })
                .then(function (response) {
                    notificationFactory.success("+1");
                    $scope.softdetail.count_likes=$scope.softdetail.count_likes+1;
                },function (response) {
                    notificationFactory.error(response.data.message);

                })
        }
        $scope.createUpdate=function (name_version,description) {
            $http({
                url: 'http://localhost:8080/soft/'+$stateParams.id+'/update',
                method: "POST",
                data: {"version": name_version,'description':description},
                headers : {'X-Auth':sessionStorage.getItem('X-Auth')}
            })
                .error(function (response) {
                    notificationFactory.error(response.message);
                })
                .then(function(response) {
                    // success
                    console.log(response);
                    $state.go("softs.update",{id:$stateParams.id,id_update:response.data.id});
                    sessionStorage.setItem('version',response.data.version);
                    notificationFactory.info("Ветка для обновления " +name_version+" создана!")
                }),
                    function (response) {
                        notificationFactory.error();
                        $rootScope.state=" Отсутствие активности, пожалуйста, авторизуйтесь заново";
                    }
        }


    })


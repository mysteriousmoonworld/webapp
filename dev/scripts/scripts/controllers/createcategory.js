/**
 * Created by Alex on 07.03.2016.
 */
angular.module('yapp')
    .controller('CreateCategoryCtrl', function($scope, $location,$http,$state,$stateParams,$rootScope,ngDialog,popup,notificationFactory) {
        var config = {
            headers : {'X-Auth':sessionStorage.getItem('X-Auth')},
        };
        $rootScope.hidenav=false;
        $rootScope.modeCreate = true;
        $rootScope.category={};
        if($stateParams.id!=null){
            $scope.idsoft=$stateParams.id;
            $rootScope.modeCreate=false;
            $http.get('http://localhost:8080/soft/'+$stateParams.id+'',config)
                .then(function(response) {

                    $rootScope.soft=response.data;
                },
                    function(response) { // optional
                        console.log(response);
                        popup.alertPopup();
                        $rootScope.state=" Отсутствие активности, пожалуйста, авторизуйтесь заново";
                        // failed
                    });
        }
        $scope.saveEdit=function (soft) {
            $state.go('softs');
        }
        $scope.saveCategory = function () {
            $http({
                    url: 'http://localhost:8080/category',
                    method: "POST",
                    data: {"name": $scope.category.name, "description": $scope.category.description},
                    headers: {'X-Auth': sessionStorage.getItem('X-Auth')}
                })
                    .then(function (response) {
                            $rootScope.idsoft=response.data.id;
                            $rootScope.namesoft=response.data.name;
                            document.forms.saveForm.reset();
                            notificationFactory.success("Категория " +$scope.category.name+" создана!")
                        },
                        function (response) { // optional
                            console.log("failed!");
                            popup.alertPopup();
                            $rootScope.state="Отсутствие активности длительное время, пожалуйста авторизуйтесь заново!";
                            // failed
                        });
            console.log($rootScope);

        }
        $scope.remaining = function () {
            if (255 - $scope.text.length >= 0)
                return 255 - $scope.text.length;
            else
                return -(255 - $scope.text.length);
        };
        $scope.shouldWarn = function () {
            return $scope.text.length > 255;
        };
    })

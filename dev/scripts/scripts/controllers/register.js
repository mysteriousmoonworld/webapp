'use strict';
/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
    .controller('RegisterCtrl', function($scope, $location,$http,notificationFactory,$sce,$mdToast) {
        localStorage.clear();
        sessionStorage.clear();
        $scope.warning="Заполните все поля формы";
        $scope.reg = function() {

            $http({
                url: 'http://localhost:8080/register',
                method: "POST",
                data: { "email":$scope.email,"pass":$scope.password }
            })
                .then(function(response) {
                    // success
                         var text=$sce.trustAsHtml("<strong class='center'>Успех! </strong><br> Пользователь "+$scope.email+" зарегистрирован");
                    ohSnap(text, {color: 'green'});
                    $location.path('/login');
                },
                function(response) { // optional

                    var text=$sce.trustAsHtml("<i class='material-icons'>error_outline</i>&nbsp;<strong class='center'>Ошибка! </strong><br> Такой пользователь уже существует");
                    ohSnap(text, {color: 'red', icon: 'icon-alert'});
                    // failed
                });

            return false;
        }

    });

/**
 * Created by Asus on 17.04.2016.
 */
angular.module('yapp')
    .directive('fileDownload', function() {
    return {
        restrict: 'E',
        template: '<a href="" class="btn btn-primary" download=""  ng-click="getfile()">Скачать</a>',
        scope: true,
        link: function (scope, element, attr) {
            var anchor = element.children()[0];

            // When the download starts, disable the link
            scope.$on('download-start', function () {
                $(anchor).attr('disabled', 'disabled');
            });

            // When the download finishes, attach the data to the link. Enable the link and change its appearance.
            scope.$on('downloaded', function (event, data) {
                $(anchor).attr({
                        download: str_rand()+'.exe'
                    })
                    .removeAttr('disabled')
                    .text('Save')
                    .removeClass('btn-primary')
                    .addClass('btn-success');

                // Also overwrite the download pdf function to do nothing.
                scope.getfile = function () {
                };
            });
        }

    }
});
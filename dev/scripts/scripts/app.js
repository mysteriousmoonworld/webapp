'use strict';

/**
 * @ngdoc overview
 * @name yapp
 * @description
 * # yapp
 *
 * Main module of the application.
 */
angular
  .module('yapp', [
    'ui.router',
    'ngAnimate',
      'ngMaterial',
      'ngDialog',
      'ngResource',
      'ncy-angular-breadcrumb',
      'toastr',
      'ngProgress',
      'ui.materialize',
      'ngSanitize'

  ],function($httpProvider)
    {

    })
    .config(function(toastrConfig) {
        angular.extend(toastrConfig, {
            allowHtml: false,
            closeButton: false,
            closeHtml: '<button>&times;</button>',
            extendedTimeOut: 3000,
            iconClasses: {
                error: 'toast-error',
                info: 'toast-info',
                success: 'toast-success',
                warning: 'toast-warning'
            },
            containerId: 'toast-container',
            newestOnTop:false,
            positionClass: 'toast-bottom-right',
            messageClass: 'toast-message',
            onHidden: null,
            onShown: null,
            onTap: null,
            progressBar: false,
            tapToDismiss: true,
            templates: {
                toast: 'directives/toast/toast.html',
                progressbar: 'directives/progressbar/progressbar.html'
            },
            timeOut: 5000,
            showEasing: "swing",
            hideEasing: "linear",
            titleClass: 'toast-title',
            toastClass: 'toast'
        });
    })
    .config(function($mdDateLocaleProvider) {
        // Example of a French localization.
        $mdDateLocaleProvider.months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
        $mdDateLocaleProvider.shortMonths = ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];
        $mdDateLocaleProvider.days = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
        $mdDateLocaleProvider.shortDays = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
        // Can change week display to start on Monday.
        $mdDateLocaleProvider.firstDayOfWeek = 1;
        // Optional.
        // Example uses moment.js to parse and format dates.
        $mdDateLocaleProvider.parseDate = function(dateString) {
            var m = moment(dateString, 'L', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        };
        $mdDateLocaleProvider.formatDate = function(date) {
            return moment(date).format('DD-MM-YYYY');
        };

        // In addition to date display, date components also need localized messages
        // for aria-labels for screen-reader users.
        $mdDateLocaleProvider.msgCalendar = 'Календарь';
        $mdDateLocaleProvider.msgOpenCalendar = 'Открыть календарь';
    })
  .config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.when('/dashboard', '/dashboard/overview');
    $urlRouterProvider.otherwise('/login');
    $stateProvider
      .state('base', {
        abstract: true,
        url: '',
        templateUrl: 'views/base.html'
      })
        .state('login', {
          url: '/login',
          parent: 'base',
          templateUrl: 'views/login.html',
          controller: 'LoginCtrl'
        })
        .state('adminDashboard', {
          url: '/admin',
          parent: 'base',
          templateUrl: 'views/admin_dashboard.html',
          controller: 'DashboardCtrl'
        })
        .state('userDashboard', {
            url: '/user',
            parent: 'base',
            templateUrl: 'views/user_dashboard.html',
            controller: 'UserDashboardCtrl'
        })
        .state('managerDashboard', {
            url: '/manage',
            parent: 'base',
            templateUrl: 'views/manager_dashboard.html',
            controller: 'ManagerDashboardCtrl'
        })
          .state('overview', {
            url: '/overview',
            parent: 'adminDashboard',
            templateUrl: 'views/admin/overview.html',
            controller: 'OverviewCtrl'
          })
        .state('register', {
            url: '/register',
            parent: 'base',
            templateUrl: 'views/register.html',
            controller: 'RegisterCtrl'
        })
        .state('softs.detail', {
            url: '/soft/:id',
            parent: 'adminDashboard',
            templateUrl: 'views/admin/soft.html',
            controller: 'ShowSoftCtrl'
        })
        .state('softs.file', {
            url: '/soft/:id_soft/update/:id_update/static',
            templateUrl: 'views/admin/soft.html',
            controller: 'ShowSoftCtrl'
        })
        .state('softs', {
            url: '/softs',
            parent: 'adminDashboard',
            templateUrl: 'views/admin/softs.html',
            controller: 'ShowSoftsCtrl',

        })
        .state('softs.create', {
            url: '/create',
            parent: 'adminDashboard',
            templateUrl: 'views/admin/create_soft.html',
            controller: 'CreateSoftCtrl',
        })
        .state('softs.editsoft', {
            url: '/soft/:id/edit',
            parent: 'adminDashboard',
            templateUrl: 'views/admin/create_soft.html',
            controller: 'CreateSoftCtrl'
        })
        .state('softs.update', {
            url: '/soft/:id/update/:id_update',
            parent: 'adminDashboard',
            templateUrl: 'views/admin/create_update.html',
            controller: 'UploadUpdateCtrl'

        })
        .state('softs.downloads', {
            url: '/soft/:id/downloads',
            parent: 'adminDashboard',
            templateUrl: 'views/admin/downloads.html',
            controller: 'DownloadSoftCtrl'
        })
        .state('categories', {
            url: '/categories',
            parent: 'adminDashboard',
            templateUrl: 'views/admin/categories.html',
            controller: 'ShowCategoriesCtrl'

        })
        .state('categories.create', {
            url: '/categories/create',
            parent: 'adminDashboard',
            templateUrl: 'views/admin/create_category.html',
            controller: 'CreateCategoryCtrl'

        })
        .state('users', {
            url: '/users',
            parent: 'adminDashboard',
            templateUrl: 'views/admin/users.html',
            controller: 'UsersCtrl'

        })
        .state('users.create', {
            url: '/users/create',
            parent: 'adminDashboard',
            templateUrl: 'views/admin/create_user.html',
            controller: 'UsersCreateCtrl'

        })
        .state('reports', {
            url: '/reports',
            parent: 'adminDashboard',
            templateUrl: 'views/admin/reports.html'
          })
      .state('softlist',{
          url:'products',
          parent : 'userDashboard',
          templateUrl:'views/user/softs.html',
          controller: 'ShowSoftsCtrl'
      })
        .state('mysoftlist',{
            url:'products/:id',
            parent : 'userDashboard',
            templateUrl:'views/user/softs.html',
            controller: 'ShowSoftsCtrl'
        })
        .state('softlist.detail', {
            url: 'product/:id',
            parent: 'userDashboard',
            templateUrl: 'views/user/soft.html',
            controller: 'ShowSoftCtrl'
        })
        .state('adminprofile', {
            url: 'admin/profile',
            parent: 'adminDashboard',
            templateUrl: 'views/admin/profile.html',
            controller: 'DashboardCtrl'
        });


  })/*
    .service('refreshToken', ['$http', function ($http) {
        this.send = function(){
            $http.get('http://localhost:8080/user/token',{headers: {'X-Auth':sessionStorage.getItem('X-Auth')}})
            .finally(function(data, status, headers, config) {
                    // success
                    sessionStorage.setItem('X-Auth',data.data.token);
                    return data;
                }
                );
        }

    }])*/
;



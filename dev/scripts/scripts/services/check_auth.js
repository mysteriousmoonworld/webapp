angular.module('yapp')
    .service('popup',function (ngDialog){
        return{
            alertPopup : function () {
                ngDialog.open({ template: 'views/alertpopup.html', className: 'ngdialog-theme-default' });               

        }}

    })
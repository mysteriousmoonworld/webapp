# Клиентская часть продукта по обновлению программного обеспечения у клиентов
## Установка проекта

1.Должно быть установлено:
    -  bower (http://bower.io/)
    -  gulp  (https://www.npmjs.com/package/gulp)
    -  npm (https://www.npmjs.org/)
    -  git
2. Клонировать репозиторий git clone git@gitlab.com:mysteriousmoonworld/webapp.git
3. В терминале выполнить
- cd `%project_directory%`
- `bower install`
- `npm install`
- `gulp` - Запуск
- `gulp build` - объединение и сжатие файлов
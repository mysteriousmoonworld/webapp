/**
 * Created by Alex on 10.03.2016.
 */
var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var bowerFiles = require('main-bower-files');
var browserSync= require('browser-sync');
var reload = browserSync.reload;
var es = require('event-stream');
var del = require('del');
var jshint = require('gulp-jslint');
var plato = require('gulp-plato');
var jscs = require("gulp-jscs");
var fixmyjs = require("gulp-fixmyjs");
var plumber = require('gulp-plumber');
var htmlmin = require('gulp-htmlmin');
var uncss = require('gulp-uncss');
//path
var paths={
    scripts: './app/**/*.js',
    lib:'./app/lib/**/*.js',
    styles: './app/styles/**/*.css',
    img: './app/images/**/*.*',
    fonts:'./app/fonts/**/*.*',
    index:'./app/index.html',
    partialas : ['./app/**/*.html','!./app/index.html'],
    distDev : './dev',
    distProd : './production'
}
//pipes
var pipes ={};
pipes.orderedVendorScripts = function(){
    return plugins.order(['jquery.js','angular.js'])
};
pipes.buildVendorScriptsDev = function(){
    return gulp.src(bowerFiles())
        .pipe(gulp.dest(paths.distDev+'/bower_components'))
};
pipes.buildLibsDev = function () {
    return gulp.src(paths.lib)
            .pipe(gulp.dest(paths.distDev+'/lib'));
}
pipes.buildAppScriptsDev = function(){
    return gulp.src(paths.scripts)

        .pipe(gulp.dest(paths.distDev+'/scripts'));
};
pipes.buildIndexDev = function(){
    var orderedVenderScripts = pipes.buildVendorScriptsDev()
        .pipe(pipes.orderedVendorScripts());
    var orderedAppScripts = pipes.buildAppScriptsDev();
    var orderedLibs=pipes.buildLibsDev();
    var appStyles = pipes.buildStylesDev();
    return gulp.src(paths.index)
        .pipe(gulp.dest(paths.distDev))
        .pipe(plugins.inject(orderedVenderScripts,{relative :true, name: 'bower'}))
        .pipe(plugins.inject(orderedAppScripts,{relative : true}))
        .pipe(plugins.inject(orderedLibs,{relative : true, name: 'lib'}))
        .pipe(plugins.inject(appStyles,{relative : true}))
        .pipe(gulp.dest(paths.distDev));
};
pipes.buildPartialsFilesDev = function(){
    return gulp.src(paths.partialas)
        .pipe(htmlmin({collapseWhitespace: true,
            removeComments: true}))
        .pipe(gulp.dest(paths.distDev));
};
pipes.processesImagesDev= function(){
    return gulp.src(paths.img)
        .pipe(gulp.dest(paths.distDev+'/images'));
};
pipes.processesFontsDev= function(){
    return gulp.src(paths.fonts)
        .pipe(gulp.dest(paths.distDev+'/fonts'));
};
pipes.buildStylesDev = function(){
    return gulp.src(paths.styles)
        .pipe(gulp.dest(paths.distDev+'/css'));
};

pipes.buildAppDev = function (){
    return es.merge(pipes.buildIndexDev(),pipes.buildPartialsFilesDev(),pipes.processesImagesDev(),pipes.processesFontsDev())
};
//tasks
gulp.task('build',pipes.buildAppDev);
gulp.task('build-app-scripts-dev',pipes.buildAppScriptsDev);
gulp.task('build-index-dev',pipes.buildIndexDev);
gulp.task('build-view',pipes.buildPartialsFilesDev);
gulp.task('clean-dev',function(){
    return del(paths.distDev)
});
gulp.task('bower', function() {
    return gulp.src(bowerFiles(), { base: './bower_components' })
        .pipe(gulp.dest(paths.distDev+'/bower_components'))
});
gulp.task('clean-build',['clean-dev'],pipes.buildAppDev)
gulp.task('watch',['clean-build'],function(){
    browserSync({
        port:3000,
        server:{
            baseDir:paths.distDev
        }
    });
    gulp.watch(paths.index,function(){
        return pipes.buildIndexDev()
            .pipe(reload({stream:true}));
    });
    gulp.watch(paths.scripts,function(){
        return pipes.buildAppScriptsDev()
            .pipe(plumber())
            .pipe(jshint())
            .pipe(fixmyjs({

                // JSHint settings here
            }))

            .pipe(reload({stream:true}));
    })
    gulp.watch(paths.partialas,function(){
        return pipes.buildPartialsFilesDev()
            .pipe(reload({stream:true}));
    })
    gulp.watch(paths.styles,function(){
        return pipes.buildStylesDev()
            .pipe(reload({stream:true}));
    })
    gulp.watch(paths.img,function(){
        return pipes.processesImagesDev()
            .pipe(reload({stream:true}))
    })
    gulp.watch(paths.fonts,function(){
        return pipes.processesFontsDev()
            .pipe(reload({stream:true}))
    })
    gulp.watch(paths.fonts,function(){
        return pipes.buildLibsDev()
            .pipe(reload({stream:true}))
    })

})

gulp.task('default',['watch']);